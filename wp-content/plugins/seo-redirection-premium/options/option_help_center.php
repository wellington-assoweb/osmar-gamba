<?php
$SR_jforms = new jforms();

$request = SRP_PLUGIN::get_request();
$app = SRP_PLUGIN::get_app();

$cat="Plugin Usage";
$name="";
$email="";
$subject="";
$details="";
$order="";

if($request->post("btn_send")!=''){

    $cat=$request->post('cat');
    $name=$request->post('name');
    $email=$request->post('email');
    $subject=$request->post('subject');
    $details=$request->post('details');
    $order=$request->post('order');

    if($request->post('name')!='' && $request->post('email')!='' && $request->post('subject')!='' && $request->post('details')!='' )
    {
        $headers = "From: $name <$email>" . "\r\n";
        wp_mail( 'support@clogica.com', $cat . ': ' . $order .': ' . $subject , $details , $headers );
        $app->echo_message("You message is received and queued, we will reply you at your email.");

        $cat="Plugin Usage";
        $name="";
        $email="";
        $subject="";
        $details="";
        $order="";

    }else
    {
        $app->echo_message("Please fill all the following fields before sending!","danger");
    }
}




?>

<style>
    #g404from .form-group{ margin-bottom: 5px;}
    #g404from p{ margin: 5px 0 5px 0;}
</style>
<h4>Help Center</h4><hr/>

<form id="g404from" action="<?php echo $request->get_current_parameters(array("add","edit","del"));?>" method="post" class="form-horizontal" role="form" data-toggle="validator">
    <div class="row">
        <div class="col-sm-12">
            <p>We recommend before contacting us and wait for reply, to explore the product page in our knowledge base, it contains many articles about how to use the plugin<br/>
                <b>To go to the <a target="_blank" href="http://www.clogica.com/kb/topics/seo-redirection-premium">knowledge base click here</a></b></p>
            <br/><br/>
            <h5 style="display: inline; color: #636465"><b>Please fill in the following form and we will reply to your e-mail.</b></h5>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10 help_icon">

            <div class="form-group">
                <label class="control-label col-sm-1" for="email">Category:</label>
                <div class="col-sm-5">
                    <?php
                        $drop = new dropdown_list('cat');
                        $drop->add('Plugin Usage', 'Plugin Usage', 'fa fa-cog');
                        $drop->add('Plugin Failure', 'Plugin Failure', 'fa fa-bug');
                        $drop->add('Suggestions', 'Suggestions', 'fa fa-envelope');
                        $drop->run($SR_jforms);
                        $drop->select($cat);
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-1" for="name">Name:</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $name; ?>" placeholder="Full Name" data-error="address is invalid" required>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-1" for="email">Email:</label>
                <div class="col-sm-7">
                    <input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>" placeholder="Valid Email (used during order)" data-error="address is invalid" required>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-1" for="order">Order Number:</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control" id="order" name="order" value="<?php echo $order; ?>" placeholder="Order Number" data-error="Order Number is invalid" required>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-1" for="subject">Subject:</label>
                <div class="col-sm-7">
                    <input type="text" class="form-control" id="subject" name="subject" value="<?php echo $subject; ?>" placeholder="Subject" data-error="address is invalid" required>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-1" for="details">Details:</label>
                <div class="col-sm-7">
                    <textarea class="form-control" rows="10" id="details" name="details"  placeholder="Write in details ..." required><?php echo $details; ?></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-1 col-sm-7">
                    <button class="btn btn-default" type="submit" name="btn_send" value="btn_send"><span class="glyphicon glyphicon-send"></span> Send</button>
                </div>
            </div>


        </div>
    </div>
</form>
<?php
$SR_jforms->hide_alerts();
$SR_jforms->run();