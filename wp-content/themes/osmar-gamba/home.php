<?php /* Template name: Blog */
get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<div class="home-blog">
	<div class="my-container">
		<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 side-desk">
                <?php include(TEMPLATEPATH . '/template-parts/search-side.php'); ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7">
                <!-- DESTAQUE -->
                <?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args = array(
                    'ignore_sticky_posts' => 1,
                    'paged' => $paged
                );
                $ultimo_post = new WP_Query( $args );
                $contDestaque = 0;
                //$notShowAgain = array();
                $i = 1;
                if($ultimo_post->have_posts()) : 
                    while( $ultimo_post->have_posts() ):
                        $ultimo_post->the_post();
                        if ($paged < 2 && $i == 1):
                            if ($contDestaque === 0):
                                $notShowAgain = $post->ID;  ?>
                                <div class="row last" itemscope itemtype="http://schema.org/BlogPosting">
                                    <div class="col-xs-12 ">
                                        <div class="thumb">
                                            <?php
                                            $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); ?>
                                            <a href="<?php echo the_permalink(); ?>">
                                                <?php if ($img_post[0]){ ?>
                                                    <img class="img-responsive" src="<?php echo $img_post[0] ?>" alt="<?php the_title(); ?>">
                                                <?php }else{ ?>
                                                    <img class="img-responsive" src="<?php echo THEMEURL.'/assets/img/default.jpg'; ?>" alt="<?php the_title(); ?>">
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="titulo-last">
                                            <h3 itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                            <?php
                                            $categoria = get_the_category();
                                            foreach($categoria as $category) {
                                                $output = '<a href="'.get_category_link( $category->term_id ).'" class="cat">'.$category->cat_name.'</a>';
                                            }?>
                                            <p class="data" >
                                                <?php the_date('d/m/Y'); ?> <i class="daniel-dot"></i> <?php echo $output; ?><span itemprop="author" class="hidden"><?php the_author_posts_link(); ?></span>
                                            </p>
                                        </div>
                                        <div class="desc-last">
                                            <a href="<?php echo the_permalink(); ?>"><?php the_content_limit(200); ?></a>
                                            <div class="btn-daniel">
                                                <div>
                                                    <a href="<?php the_permalink(); ?>"><span>Continuar lendo</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $contDestaque++;
                            endif;
                        endif; 
                        if($notShowAgain != $post->ID): ?>
                            <div class="col-xs-12 ultimas <?php if($i %2 == 1) echo 'gray';?>">
                                <div class="thumb">
                                    <?php
                                    $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); ?>
                                    <a href="<?php echo the_permalink(); ?>">
                                        <?php if ($img_post[0]){ ?>
                                            <img class="img-responsive" src="<?php echo $img_post[0] ?>" alt="<?php the_title(); ?>">
                                        <?php }else{ ?>
                                            <img class="img-responsive" src="<?php echo THEMEURL.'/assets/img/default.jpg'; ?>" alt="<?php the_title(); ?>">
                                        <?php } ?>
                                    </a>
                                </div>
                                <div class="titulo-last">
                                    <h3 itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <?php
                                    $categoria = get_the_category();
                                    foreach($categoria as $category) {
                                        $output = '<a href="'.get_category_link( $category->term_id ).'" class="cat">'.$category->cat_name.'</a>';
                                    }?>
                                    <p class="data" >
                                        <?php the_date('d/m/Y'); ?> <i class="daniel-dot"></i> <?php echo $output; ?><span itemprop="author" class="hidden"><?php the_author_posts_link(); ?></span>
                                    </p>
                                </div>
                                <div class="desc-last">
                                    <?php the_content_limit(200); ?>
                                    <div class="btn-daniel">
                                        <div>
                                            <a href="<?php the_permalink(); ?>"><span>Continuar lendo</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; 
                        endif; ?>
                    <?php endwhile; ?>
                    <div class="navegacao">
                        <?php
                            if (function_exists(custom_pagination)) {
                                custom_pagination($posts_query->max_num_pages,"",$paged);
                            }
                        ?>
                    </div>
                <?php endif;?>
            </div>
            <div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-0 col-md-4 col-lg-3 side-mobile">
                <?php include(TEMPLATEPATH . '/template-parts/destaques.php'); ?>
            </div>
		</div>
	</div>
</div>
<?php get_footer(); ?>