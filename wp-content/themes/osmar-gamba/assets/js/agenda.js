(function($) {
    var contClick = 0;

    // OPEN A EVENT
	$('.event').on('click', function() {
        if (contClick == 0) {
            if($(this).hasClass('open')){
            }else{
                contClick++;
                //initializeMaps();
            }
        }

        selecionado = $(this).next('.mapa');
        console.log(selecionado);

        $('.event').removeClass('open');
        $(this).toggleClass('open');

    });


	var total = parseInt($( '#total' ).text()); //QUANTIDADE DE EVENTOS
	var map = null;
	var marcador, myMap;


    function new_map( el, ponto ) {
        var markers = el.find(ponto);

        var args = {
            zoom        : 16,
            center      : new google.maps.LatLng(markers),
            mapTypeId   : google.maps.MapTypeId.ROADMAP
        };                                              
                 
        var mapa = new google.maps.Map( el[0], args);
                                                    
        // add a markers reference
        mapa.markers = [];
                                                    
        // add markers
        markers.each(function(){
            add_marker( $(this), mapa ); //Adiciona marcador
        });
    
        // center map
        center_map( mapa );
    
        // return
        return mapa;
    }

    function add_marker( $marker, map ) {
        search = [$marker.attr('data-lat'), $marker.attr('data-lng')];
        console.log(search);

        var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
        var marker = new google.maps.Marker({
            position    : latlng,
            map         : map
        });


        // add to array
        map.markers.push( marker );

        // if marker contains HTML, add it to an infoWindow
        if( $marker.html() ){
            // create info window
            var infowindow = new google.maps.InfoWindow({
                content     : $marker.html()
            });

            // show info window when marker is clicked
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open( map, marker );
            });

            
        }else{



        }

    }

    function center_map( map ) {
        var bounds = new google.maps.LatLngBounds();

        // loop through all markers and create bounds
        $.each( map.markers, function( i, marker ){
            var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
            bounds.extend( latlng );
        });

        // only 1 marker?
        if( map.markers.length >= 1 ){
            // set center of map
            map.setCenter( bounds.getCenter() );
            map.setZoom( 16 );

        }else{
            // fit to bounds
            map.fitBounds( bounds );
        }
    }

    function initializeMaps() {
        for (var i = 0; i < total; i++) {
            marcador = $('#marker'+i);
            myMap = $('#mapa'+i);
            //removeMarkers(marcador);

            $(myMap).each(function(){    
                map = new_map(myMap, marcador); // create map
            });
        }
    }

    /* MAPAS */
    $(document).ready(function(){
        initializeMaps();
    });

})(jQuery);