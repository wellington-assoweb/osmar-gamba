(function($) {
	var propostasMenu = $('.menu-proposta .owl-carousel');
	
	$(document).ready(function(){
		propostasMenu.owlCarousel({
	        items:1,
	        video:true,
	        lazyLoad:true,
	        responsive:{
				0:{
					items: 1
				},
				481:{
					items: 1
				},
				790:{
				    items: 2
				},
				991:{
					items: 3
				},
				1199:{
					items: 3
				},
				1351:{
					items: 4
				},
				1480:{
					items: 4
				},
				1920:{
					items: 5
				}
	        }
        });
    });

	$('.item-proposta a').click(function(event) {
		event.preventDefault();
		if ($('.item-proposta a').hasClass('active')){
			$('.item-proposta a').removeClass('active');
		} 
		$(this).addClass('active');

	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
	        || location.hostname == this.hostname) {

	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	           if (target.length) {
	             $('html,body').animate({
	                 scrollTop: target.offset().top-70
	            }, 1000);
	            return false;
	        }
	    }
	});

	$('.navega').click(function(event) {
		event.preventDefault();

	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
	        || location.hostname == this.hostname) {

	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	           if (target.length) {
	             $('html,body').animate({
	                 scrollTop: target.offset().top-70
	            }, 1000);
	            return false;
	        }
	    }
	});

	//SMALLER HEADER WHEN SCROLL PAGE
    $(window).scroll(function () {
    	var heightHead = $('.head-title').height();
    	heightHead = heightHead+70;
        var sc = $(window).scrollTop()
        if (sc > (heightHead)) {
    		console.log('fixed');
            $('.menu-proposta').addClass('fixed')
        }else {
    		console.log('no-fixed');
            $('.menu-proposta').removeClass('fixed')
        }
    }); 

})(jQuery);