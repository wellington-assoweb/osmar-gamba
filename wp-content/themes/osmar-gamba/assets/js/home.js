(function($) {
	var alturaBanner;
	alturaBanner = $('#full').outerHeight();
	jQuery(window).resize(function() {
		alturaBanner = $('#full').outerHeight();
	});

	var // Declaração de slides
	videoSlide = $('.slide-videos .owl-carousel'),
	listaVideo = $('.control-slide .owl-carousel'),
	depoimentoSlide = $('.depoimentos .owl-carousel'),
	theDepoi = $('.dizeres .owl-carousel');
	photoSlide = $('.last-photos .owl-carousel');
	blogSlide = $('.slide-blog .owl-carousel');


	$('.control-slide .lista-video .item').on('click', function() {
		console.log('clicou');
		var item = $(this);
		var slide = item.data('slide');
		if ($('.control-slide .lista-video .owl-item .item').hasClass('selecionado')){
			$('.control-slide .lista-video .owl-item .item').removeClass('selecionado');
		}
		$(this).addClass('selecionado');
		videoSlide.trigger('to.owl.carousel', [slide, 700, true]);
		listaVideo.trigger('to.owl.carousel', [slide, 700, true]);
	});

	$(document).ready(function(){

		videoSlide.owlCarousel({
	        //items:1,
	        merge:true,
	        loop:true,
	        video:true,
	        lazyLoad:true,
	        center:true,
	        responsive:{
				0:{
					items: 1
				},
				481:{
					items: 1
				},
				790:{
				    items: 2
				},
				991:{
					items: 3
				},
				1199:{
					items: 3
				},
				1351:{
					items: 3
				},
				1480:{
					items: 3
				},
				1920:{
					items: 5
				}
	        },
	        navText: ['<i class="osmar-left-open"></i>','<i class="osmar-right-open"></i>']
        });

		listaVideo.owlCarousel({
	        //items:1,
	        merge:true,
	        loop: true,
	        smartSpeed: 1000,
	        lazyLoad:true,
	        center:true,
	        responsive:{
				0:{
					items: 1
				},
				481:{
					items: 2
				},
				790:{
				    items: 3
				},
				991:{
					items: 4
				},
				1199:{
					items: 4
				},
				1351:{
					items: 5
				},
				1480:{
					items: 5
				},
				1920:{
					items: 5
				}
	        },
			navText: ['<i class="osmar-left-open"></i>','<i class="osmar-right-open"></i>']
	    });

	    depoimentoSlide.owlCarousel({
	        items: 5,
	        loop: true,
	        merge:true,
	        //autoplay: true,
	        smartSpeed: 1300,
			autoplayTimeout:10000,
	        lazyLoad:true,
	        center:true,

	        responsive:{
				0:{
					items: 1
				},
				481:{
					items: 3
				},
				790:{
				    items: 3
				},
				991:{
					items: 5
				},
				1199:{
					items: 5
				},
				1351:{
					items: 5
				},
				1480:{
					items: 5
				},
				1920:{
					items: 5
				}
	        },
				navText: ['<i class="osmar-left-open"></i>','<i class="osmar-right-open"></i>']
	    });

	  //   theDepoi.owlCarousel({
	  //       items: 1,
	  //       loop: true,
	  //       merge:true,
	  //       //autoplay: true,
	  //       autoHeight: true,
			// autoplayTimeout:10000,
	  //       smartSpeed: 1300,
	  //       lazyLoad:true,
	  //   	animateIn: 'fadeIn',
	  //   	animateOut: 'fadeOut',
			// navText: ['','']
	  //   });

		photoSlide.owlCarousel({
	        items: 1,
	        merge: true,
	        loop: true,
	        smartSpeed: 1300,
			autoplayTimeout:11000,
	    	animateIn: 'fadeIn',
	    	animateOut: 'fadeOut',
			autoplay: true,
	        lazyLoad: true
	    });
	});

	// Nagegação depoimentos
	$('.next-btn').click(function() {
		depoimentoSlide.trigger('next.owl.carousel', [1000]);
		theDepoi.trigger('next.owl.carousel', [1000]);
	});
	$('.prev-btn').click(function() {
	    depoimentoSlide.trigger('prev.owl.carousel', [1000]);
	    theDepoi.trigger('prev.owl.carousel', [1000]);
	});


	// if ($('.dizem-slide .owl-carousel .owl-item.active.center').length) {
	// 	console.log('tô dentro');
	// }
	// if( $('.dizem-slide .owl-carousel .owl-item.active.center').exists ){
	// 	console.log('tô dentro');
	// 	var item = $('.dizem-slide .owl-carousel .owl-item.active.center');
	// 	var slide = item.data('slide');
	// 	if(slide != ''){
	// 		depoimentoSlide.trigger('to.owl.carousel', [slide, 700, true]);
	// 		theDepoi.trigger('to.owl.carousel', [slide, 700, true]);
	// 	}
	// }else{
	// 	console.log('deu não');
	// }

})(jQuery);

/* Vídeo banner */
// var videoID = document.getElementById('video-id').innerHTML;
// console.log(videoID);
// var tag = document.createElement('script');
// tag.src = 'https://www.youtube.com/player_api';
// var firstScriptTag = document.getElementsByTagName('script')[0];
// firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
// var tv, playerDefaults = {autoplay: 0, autohide: 1, modestbranding: 0, rel: 0, showinfo: 0, controls: 0, disablekb: 1, enablejsapi: 0, iv_load_policy: 3, allowfullscreen: 1};
// var vid = [
// 	{'videoId': videoID, 'startSeconds': 0, 'endSeconds': 90, 'suggestedQuality': 'hd720'},
// ],
// randomvid = Math.floor(Math.random() * (vid.length - 1 + 1));

// function onYouTubePlayerAPIReady(){
//   	tv = new YT.Player('video', {
//   		events: {'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange},
//   		playerVars: playerDefaults
//   	});
// }

// function onPlayerReady(){
// 	tv.loadVideoById(vid[randomvid]);
// 	tv.mute();
// }

// function onPlayerStateChange(e) {
// 	if (e.data === 1){
// 		//$('#tv').addClass('active');
// 	} else if (e.data === 0){
// 		tv.seekTo(vid[randomvid].startSeconds)
// 	}
// }
