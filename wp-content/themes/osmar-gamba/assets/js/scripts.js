(function($) {

    // Animate responsive menu
    $('.hamburger-menu').on('click', function() {
        $('.bar').toggleClass('animate');
        if( $('body').hasClass('open-form')){
            $('body').removeClass('open-form');
        }else{
            $('body').toggleClass('open-menu');
        }
    });

    // Animate responsive menu
    $('.speak').on('click', function(event) {
        event.preventDefault();
        $('.bar').toggleClass('animate');
        $('body').toggleClass('open-form');
    });

    // When press esc
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $('.bar').removeClass('animate');
            $('body').removeClass('open-menu');
            $('body').removeClass('open-form');
        }
    });

    //SMALLER HEADER WHEN SCROLL PAGE
    $(window).scroll(function () {
        var sc = $(window).scrollTop();
        var sc = $(window).scrollTop();
        if (sc > 80) {
            $('.header').addClass('small');
        }else {
            $('.header').removeClass('small');
        }
    });

    // UPDATE YOUR BROWSER
    var $buoop = {vs:{i:11,f:30,o:15,s:7},c:2};
    function $buo_f(){
        var e = document.createElement("script");
        e.src = "//browser-update.org/update.min.js";
        document.body.appendChild(e);
    };
    try {
        document.addEventListener("DOMContentLoaded", $buo_f,false)
    }catch(e){
        window.attachEvent("onload", $buo_f)
    }
    // END UPDATE YOUR BROWSER


    $('a[href*=#L]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
            if($('#searchbar').hasClass('aparece')) {
                $('#searchbar').removeClass('aparece').addClass('some');
            }else{
                $('#searchbar').removeClass('some').addClass('aparece');
            }
        }
    });

    //Hamburger menu addClass for animate on DESKTOP
    $('#segdesk').on('click', function() {
        $('.bar').toggleClass('animate');
        if ($('#theSeg').hasClass('shownow')) {
            $('#theSeg').removeClass('shownow');
            $('#theSeg').addClass('hiddnow');
        }else{
            $('#theSeg').removeClass('hiddnow');
            $('#theSeg').addClass('shownow');
        }
    });

    //Hamburger menu addClass for animate on MOBILE
    $('#segmobi').on('click', function() {
        $('.bar').toggleClass('animate');
        if ($('#theSegResp').hasClass('shownow')) {
            $('#theSegResp').removeClass('shownow');
            $('#theSegResp').addClass('hiddnow');
        }else{
            $('#theSegResp').removeClass('hiddnow');
            $('#theSegResp').addClass('shownow');
        }
    })

    //SMALLER HEADER WHEN SCROLL PAGE
    $(window).scroll(function () {
        var sc = $(window).scrollTop();
        if (sc > 80) {
            console.log('teste');
            $('#header-home').addClass('small');
        }else {
            $('#header-home').removeClass('small');
        }
    });

    //KEEP THE FOOTER IN FOOTER
    function rodape(){
        var footerHeight = $('.footer').height();
        $('.footer').css('margin-top', -(footerHeight)+"px");
        //$('.conteudo').css('padding-bottom', (footerHeight )+"px");
    };

    //RESIZE BANNER FOR FULLSCREEN
    function full_screen_section(){
        var windowHeight = jQuery(window).height();
        $('.primary').css('height', windowHeight);
        $('.owl-carousel').each(function(){
            var seg = $('.segmentation').height();
            $('.slide .item').css('height', windowHeight-seg+"px");
        });
    };

    /* MENU SCRIPT */
    var openbtn = document.getElementById( 'open-button' ),
        headscroll = document.getElementById( 'header-sroll' ),
        isOpen = false;

    function toggleMenu() {
        if( isOpen ) {
            classie.remove( headscroll, 'show-menu' );
        }
        else {
            classie.add( headscroll, 'show-menu' );
        }
        isOpen = !isOpen;
    }
    //init();
    /* END MENU SCRIPT */


    $(document).ready(function(a){
        rodape();
        /* SCROLL TO TOP*/
        var b=300,c=1200,d=700,e=a(".cd-top");a(window).scroll(function(){a(this).scrollTop()>b?e.addClass("cd-is-visible"):e.removeClass("cd-is-visible cd-fade-out"),a(this).scrollTop()>c&&e.addClass("cd-fade-out")}),e.on("click",function(b){b.preventDefault(),a("body,html").animate({scrollTop:0},d)})
    });

    jQuery(window).resize(function() {
        rodape();
    });

    $(document).ready(function(a){

    });


})(jQuery);