<?php /* Template name: Página Inicial */ get_header();  $imagem_do_banner = get_field('imagem_do_banner');?>

<div class="banner capa<?php echo $imagem_do_banner->ID; ?>" id="full">
	<div class="over"></div>
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="box">
					<img src="<?php echo THEMEURL ?>/assets/img/logotipo.png" alt="Osmar Gambá">
					<h2>Coromandel não quer <br>andar pra trás!</h2>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $imagem = get_field('imagem_sobre_osmar'); ?>
<section class="sobre" style="background-image:url('<?php echo $imagem['url']?>');">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="title">
					<h2>Quem é <span>Osmar Gambá</span></h2>
				</div>
				<div class="desc">
					<?php the_field('sobre_oemar'); ?>
				</div>
				<div class="btn-osmar">
					<a href="<?php echo SITEURL.'/biografia'; ?>"><span>Saiba <b>mais</b></span></a>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="title">
					<h3>Principais propostas</h3>
				</div>
			</div>
			<div class="propostas">
				<?php
					// $propostas = new WP_Query("page_id=498");
					// $j = 1;
					// while($propostas->have_posts()) : $propostas->the_post();
					// 	if( have_rows('propostas') ):
					// 	 	while ( have_rows('propostas') ) : the_row();
					//         	if( get_row_layout() == 'proposta' ): ?>
					 				<!-- <a href="<?php //echo SITEURL.'/propostas/#proposta'.$j; ?>"><?php the_sub_field('nome_proposta'); ?></a> -->
					 			<?php //endif;
					// 			$j++;
					// 		endwhile;
					// 	endif;
					// endwhile;
				?>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="btn-osmar">
						<a href="<?php echo SITEURL.'/biografia'; ?>"><span>Ótima Proposta</span></a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="btn-osmar">
						<a href="<?php echo SITEURL.'/biografia'; ?>"><span>Ótima Proposta</span></a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="btn-osmar">
						<a href="<?php echo SITEURL.'/biografia'; ?>"><span>Ótima Proposta</span></a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="btn-osmar">
						<a href="<?php echo SITEURL.'/biografia'; ?>"><span>Ótima Proposta</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="some-videos">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="title">
					<h2>TV <span>Osmar</span></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="slide-videos">
		<div class="owl-carousel">
			<?php
	        $videos = array('post_type' => 'midia',  'order' => 'ASC', 'posts_per_page' => -1 );
	        $video = new WP_Query( $videos );
	 		while ( $video->have_posts() ) : $video->the_post();
				$terms = get_the_terms($post->ID, 'categorias-midia' );
	            if ($terms && ! is_wp_error($terms)) :
	                $term_slugs_arr = array();
	                foreach ($terms as $term) {
	                    $term_slugs_arr[] = $term->name;
	                }
	                $terms_slug_str = join( " ", $term_slugs_arr);
	            endif;
	            $docat = $terms_slug_str;
	            if ($docat == 'Vídeos'): ?>
					<div class="item-video">
				    	<div class="over"></div>
				    	<a class="owl-video" href="https://www.youtube.com/watch?v=<?php the_field('id_video'); ?>"></a>
				    </div>
	            <?php endif; ?>
			<?php endwhile;
	        wp_reset_query(); ?>
		</div>
	</div>
</section>
<section class="depoimentos">
	<div class="my-container">
		<div class="col-xs-12">
			<div class="desc">
				<h2>Depoimentos</h2>
				<div class="dizeres">
					<div class="owl-carousel">
						<?php
							if( have_rows('depoimentos') ):
								$j = 0;
							    while ( have_rows('depoimentos') ) : the_row();
							    	$foto_depoente = get_sub_field('foto_depoente');
							        if( get_row_layout() == 'depoimento' ): ?>
										<div class="item" data-slide="<?php echo $j; ?>">
											<img src="<?php echo $foto_depoente['url']; ?>" alt="">
											<h4><?php the_sub_field('nome_depoente'); ?></h4>
											<?php the_sub_field('depoimento'); ?>
										</div>
							        	<?php $j++;
							        endif;
							    endwhile;
							endif;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="last-blog">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="titulo">
					<h2>Últimas <span>do Blog</span></h2>
				</div>
			</div>
			<?php
			$artigo = array(
				'post_type' => 'post',
				'posts_per_page' => 6
				//, 'post__not_in' => get_option( 'sticky_posts')
			);
			$artigos = new WP_Query( $artigo );
			$conta = 1;
			$time = 0.2;
			while ( $artigos->have_posts() ) : $artigos->the_post();
				if ($conta >= 4):
					break;
				endif;
				if ($conta === 3): ?>
					<div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-0 col-md-4">
				<?php else: ?>
				<div class="col-xs-12 col-sm-6 col-md-4">
				<?php endif; ?>
					<div class="post wow fadeInUp" data-wow-duration="1s" data-wow-delay="<?php echo $time; ?>s">
						<div class="image">
							<div class="post-<?php echo $post->ID; ?>"></div>
						</div>
						<div class="body-post">
							<div class="inf">
								<span class="date"><?php the_date('d/m/Y'); ?></span> -
								<span class="category">
									<?php $categories = get_the_category();
										$output = '';
										$i = 0;
										if($categories){
											foreach($categories as $category) {
												if ($i == 1) {
													break;
												}
												$output .= '<a href="'.get_category_link( $category->term_id ).'" title="'.$category->cat_name.'">'.$category->cat_name.'</a>';
												$i++;
											}
											echo trim($output);
										}
									?>
								</span>
							</div>
							<div class="title">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							</div>
							<div class="desc">
								<?php the_content_limit(80); ?>
							</div>
						</div>
					</div>
				</div>
				<?php $conta++;
				$time+=0.2;
			endwhile;
			wp_reset_query(); ?>
		</div>
	</div>
</section>
<section class="last-photos">
	<div class="owl-carousel">
		<?php
        $galerias = array('post_type' => 'midia',  'order' => 'ASC', 'posts_per_page' => -1 );
        $galeria = new WP_Query( $galerias );
 		while ( $galeria->have_posts() ) : $galeria->the_post();
			$terms = get_the_terms($post->ID, 'categorias-midia' );
            if ($terms && ! is_wp_error($terms)) :
                $term_slugs_arr = array();
                foreach ($terms as $term) {
                    $term_slugs_arr[] = $term->name;
                }
                $terms_slug_str = join( " ", $term_slugs_arr);
            endif;
            $docat = $terms_slug_str;
            if ($docat == 'Imagens'): ?>
				<div class="photo galery<?php echo $post->ID; ?>">
					<div class="desc">
						<?php the_content_limit(350); ?>
					</div>
					<div class="over"></div>
				</div>
            <?php endif ?>
		<?php endwhile;
        wp_reset_query(); ?>
	</div>
	<div class="title">
		<h2>Imagens <span>recentes</span></h2>
	</div>
</section>
<?php get_footer(); ?>