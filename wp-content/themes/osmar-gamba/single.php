<?php get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<div class="home-blog single-blog">
	<div class="my-container">
		<div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 side-desk">
                <?php include(TEMPLATEPATH . '/template-parts/search-side.php'); ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7">
            	<div class="image-destacada">
					<?php
	                $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
	                if ($img_post[0]){ ?>
	                    <img class="img-responsive" src="<?php echo $img_post[0] ?>" alt="<?php the_title(); ?>">
	                <?php }else{ ?>
	                    <img class="img-responsive" src="<?php echo THEMEURL.'/assets/img/default.jpg'; ?>" alt="<?php the_title(); ?>">
	                <?php } ?>
            	</div>
            	<!-- <div class="title">
            		<h1><?php the_title(); ?></h1>
            	</div> -->
            	<div class="dat-cat">
	            	<?php
	                $categoria = get_the_category();
	                foreach($categoria as $category) {
	                    $output = '<a href="'.get_category_link( $category->term_id ).'" class="cat" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
	                }?>
	                <p class="data" ><?php the_date('d/m/Y'); ?> <i class="daniel-dot"></i> <?php echo $output; ?><span itemprop="author" class="hidden"><?php the_author_posts_link(); ?></span></p>
            	</div>
            	<div class="contem">
            		<?php the_content(); ?>
            	</div>
            	<div class="comentarios">
                    <?php disqus_embed('danielannenberg'); ?>
                    <?php /*comments_template();*/ ?>
                </div>
            	<div class="navgation">
	            	<?php
						next_post_link( $format, $link, $in_same_term = false, $excluded_terms = '', $taxonomy = 'category' );
					?>
            	</div>
            </div>
            <div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-0 col-md-4 col-lg-3 side-mobile">
                <?php include(TEMPLATEPATH . '/template-parts/destaques.php'); ?>
            </div>
		</div>
	</div>
</div>
<?php get_footer(); ?>