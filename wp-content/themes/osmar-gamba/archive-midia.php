<?php get_header(); /* Template name: Mídia */ ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>

<div class="midia archive">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">                
                <div class="grid-full">
                    <main class="cd-main-content">
                        <div class="cd-tab-filter-wrapper">
                            <div class="cd-tab-filter">
                                <ul class="cd-filters">
                                    <?php
                                    //Menu Select 
                                    $args = array(
                                        'taxonomy' => 'categorias-midia'
                                    );
                                    $categories = get_categories($args);
                                    $totalCategorias= count($categories);
                                    foreach ($categories as $cat) {
                                        if ($cat->category_nicename == 'imagens') {
                                            $option = '<li class="filter" data-filter=".'.$cat->category_nicename.'" type="checkbox" id="checkbox'.$cat->term_id.'"><a  href="#0" class="selected" data-type="'.$cat->category_nicename.'">'.$cat->cat_name.'</a></li>';
                                            echo $option;
                                        }else{
                                            $option = '<li class="filter" data-filter=".'.$cat->category_nicename.'" type="checkbox" id="checkbox'.$cat->term_id.'"><a href="#0" data-type="'.$cat->category_nicename.'">'.$cat->cat_name.'</a></li>';
                                            echo $option;
                                        }
                                    } ?>
                                </ul>
                            </div>
                        </div>
                    
                        <div class="cd-gallery">
                            <ul class="galeria">
                                <?php
                                $args = array(
                                    'post_type'      => 'midia',
                                    //'order'          => 'DESC',
                                    'posts_per_page' => -1
                                );
                                $loop = new WP_Query( $args );
                                while ( $loop->have_posts() ) : $loop->the_post();
                                    $categorias = get_the_terms($post->ID, 'categorias-midia');
                                    foreach ($categorias as $cat) {
                                        $cats.= $cat->slug.' ';
                                    };
                                    if(strpos($cat->slug,'imagens') !== false): ?>
                                        <li class="mix imagens">
                                            <div class="item-fotografias">
                                                <div class="imagem capa<?php echo $post->ID; ?>">
                                                    <a href="<?php the_permalink(); ?>" title="<?php the_title();?>" class="linka"></a>
                                                    <div class="overlay-servicos"></div>
                                                </div>
                                                <p><a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php the_title();?></a></p>
                                            </div>
                                        </li>
                                    <?php else: ?>
                                        <li class="mix <?php echo $cats;?> media section odd" style="display: none !important;" >
                                            <div class="item-fotografias">
                                                <div class="imagem capa<?php echo $post->ID; ?> video">
                                                    <img src="http://img.youtube.com/vi/<?php the_field('id_video'); ?>/mqdefault.jpg" alt="<?php the_title();?>">
                                                    <a href="https://www.youtube.com/watch?v=<?php the_field('id_video'); ?>" class="btnplay"></a>
                                                    <i class="daniel-play-circled2"></i>
                                                </div>
                                                <p><a href="https://www.youtube.com/watch?v=<?php the_field('id_video'); ?>" class="btnplay" title="<?php the_title();?>"><?php the_title();?></a></p>
                                            </div>
                                        </li>
                                    <?php  endif; ?>
                                    <?php $cats = '';
                                endwhile; ?>
                            </ul>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
<!-- MODAL DOS VÍDEOS -->
<div class="modal the-mod fade" id="video" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <div id="player"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="daniel-cancel-circled-outline"></i></button>
            </div>
        </div>
    </div>
</div>