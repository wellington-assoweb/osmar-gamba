<div class="destaques">
	<div class="title">
		<h3>Destaques</h3>
	</div>
    <?php
    $args2 = array(
        'posts_per_page' => 3,
        'post__in'  => get_option( 'sticky_posts' ),
        'ignore_sticky_posts' => false,              // Ignora posts fixos
        'orderby'             => 'meta_value_num',  // Ordena pelo valor da post meta
        'order'               => 'DESC'             // Ordem decrescente
    );
    $destaques = new WP_Query( $args2 );

    if($destaques->have_posts()){?>
    <div class="row">
        <?php while( $destaques->have_posts() ) {
        $destaques->the_post();?>

        <div class="col-xs-12 destaque">
            <div class="thumb">
                <?php
                $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); ?>
                <a href="<?php echo the_permalink(); ?>">
                    <?php if ($img_post[0]){ ?>
                        <img class="img-responsive" src="<?php echo $img_post[0] ?>" alt="<?php the_title(); ?>">
                    <?php }else{ ?>
                        <img class="img-responsive" src="<?php echo THEMEURL.'/assets/img/default.jpg'; ?>" alt="<?php the_title(); ?>">
                    <?php } ?>
                </a>
            </div>
            <div class="titulo-last">
                <h3 itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                <?php
                $categoria = get_the_category();
                foreach($categoria as $category) {
                    $output = '<a href="'.get_category_link( $category->term_id ).'" class="cat" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
                }?>
                <p class="data" ><?php the_date('d/m/Y'); ?> <i class="daniel-dot"></i> <?php echo $output; ?><span itemprop="author" class="hidden"><?php the_author_posts_link(); ?></span></p>
	            <div class="desc-last">
	                <div class="btn-daniel">
                        <div>
	                       <a href="<?php the_permalink(); ?>" title="Leia mais"><span>Leia mais</span></a>
                        </div>
	                </div>
	            </div>
            </div>
            </div>
        <?php }?>
    </div>
    <?php }?>
</div>