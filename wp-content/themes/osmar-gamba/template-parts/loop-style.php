<?php
echo '<style>';
	if (is_front_page()):
		// $backgournd = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
		$banner = get_field('imagem_do_banner');
		// $img_destak = $backgournd['0'];
		if ($banner) {
			echo '.capa'.$banner->ID.'{background-image:url('.$banner['url'].')}';
			// echo '.capa'.$post->ID.'{background-image:url('.$img_destak.')}';
		}else{
			echo '.capa'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default.jpg)}';
		}



		// Background posts
		$post = array('post_type' => 'post', 'posts_per_page' => 3 );
		$posts = new WP_Query( $post );
		while ( $posts->have_posts() ) : $posts->the_post();
			$backgournd = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
			$img_destak = $backgournd['0'];
			if ($img_destak) {
				echo '.image .post-'.$post->ID.'{background-image:url('.$img_destak.')}';
			}else{
				echo '.image .post-'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default.jpg)}';
			}
		endwhile;
		wp_reset_query();

		// Background Galeria de imagens recentes
        $galerias = array('post_type' => 'midia',  'order' => 'ASC', 'posts_per_page' => -1 );
        $galeria = new WP_Query( $galerias );
 		while ( $galeria->have_posts() ) : $galeria->the_post();
			$terms = get_the_terms($post->ID, 'categorias-midia' );
            if ($terms && ! is_wp_error($terms)) :
                $term_slugs_arr = array();
                foreach ($terms as $term) {
                    $term_slugs_arr[] = $term->name;
                }
                $terms_slug_str = join( " ", $term_slugs_arr);
            endif;
            $categoiaImg = $terms_slug_str;
            if ($categoiaImg == 'Imagens'):
	            $backgournd = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$img_destak = $backgournd['0'];
				if ($img_destak) {
					echo '.photo.galery'.$post->ID.'{background-image:url('.$img_destak.')}';
				}else{
					echo '.photo.galery'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default.jpg)}';
				}
            endif;
		endwhile;
        wp_reset_query();

	elseif(is_home()):

		$post = array('post_type' => 'post', 'posts_per_page' => 3 );
		$posts = new WP_Query( $post );
		while ( $posts->have_posts() ) : $posts->the_post();
			$backgournd = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
			$img_destak = $backgournd['0'];
			if ($img_destak) {
				echo '.image .post-'.$post->ID.'{background-image:url('.$img_destak.')}';
			}else{
				echo '.image .post-'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default.jpg)}';
			}
		endwhile;
		wp_reset_query();

	elseif(is_post_type_archive('agenda') || is_singular('agenda')):

		if (is_post_type_archive('agenda')):
	        $agenda = array('post_type' => 'agenda',  'order' => 'ASC', 'posts_per_page' => -1 );
	        $mes = new WP_Query( $agenda );
	        while ( $mes->have_posts() ) : $mes->the_post();
				if( have_rows('sobre_evento') ):
			        $cont = 0;
			        while ( have_rows('sobre_evento') ) : the_row();
			            if( get_row_layout() == 'evento' ):
							$capaEvento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
							$img_destak = $capaEvento['0'];
							if ($img_destak) {
								echo '.cover-'.$cont.'{background-image:url('.$img_destak.')}';
							}else{
								echo '.cover-'.$cont.'{background-image:url('.THEMEURL.'/assets/img/default.jpg)}';
							}
							$cont++;
			            endif;
			        endwhile;
			    endif;
	        endwhile;
	        wp_reset_query();
		elseif (is_singular('agenda')):
			if(have_rows('sobre_evento')):
		        $cont = 0;
		        while ( have_rows('sobre_evento') ) : the_row();
		            if( get_row_layout() == 'evento' ):
						$capaEvento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
						$img_destak = $capaEvento['0'];
						if ($img_destak) {
							echo '.cover-'.$cont.'{background-image:url('.$img_destak.')}';
						}else{
							echo '.cover-'.$cont.'{background-image:url('.THEMEURL.'/assets/img/default.jpg)}';
						}
						$cont++;
		            endif;
		        endwhile;
		    endif;
		endif;

	elseif(is_post_type_archive('midia') || is_singular('midia')):

        $categorias = array('post_type' => 'midia',  'order' => 'ASC', 'posts_per_page' => -1 );
        $categoria = new WP_Query( $categorias );
        $le = 1;
        while ( $categoria->have_posts() ) : $categoria->the_post();
        	if (has_post_thumbnail($post->ID )):
                $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
                $url_img = $img_post['0'];
				echo '.capa'.$post->ID.'{background-image:url('.$url_img.')}';
            else:
				echo '.capa'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default.jpg)}';
			endif;
  		endwhile;
        wp_reset_query();

	elseif(is_page(219)):

        $args = array(
            'post_type'      => 'midia',
            'order'          => 'DESC',
            'posts_per_page' => -1
        );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
        	if (has_post_thumbnail($post->ID )):
                $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
                $url_img = $img_post['0'];
				echo '.capa'.$post->ID.'{background-image:url('.$url_img.')}';
            else:
				echo '.capa'.$post->ID.'{background-image:url('.THEMEURL.'/assets/img/default.jpg)}';
			endif;

		endwhile;
        wp_reset_query();

	endif;
echo '</style>'; ?>
