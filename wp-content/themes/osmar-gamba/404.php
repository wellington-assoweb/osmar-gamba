<?php get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<section class="the-error">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="search404">
					<h2>Infelizmente, não encontramos essa página.</h2>
					<div class="error">
						<p>Talvez o menu acima tenha o que você procura!</p>
					</div>
				</div>
			</div>			
		</div>
	</div>
</section>
<section class="propostas">
	<div class="my container">
		<div class="row">
			<div class="col-xs-12">
				<div class="title">
					<h2>Veja nossas <span>propostas</span></h2>
				</div>
				<div class="proposites">
					<div class="box">
						<?php $propostas = new WP_Query("page_id=498"); 
						$j = 1;
						while($propostas->have_posts()) : $propostas->the_post();
							if( have_rows('propostas') ):
							 	while ( have_rows('propostas') ) : the_row();
						        	if( get_row_layout() == 'proposta' ): ?>
										<a href="<?php echo SITEURL.'/propostas/#proposta'.$j; ?>"><?php the_sub_field('nome_proposta'); ?></a>
									<?php endif;
									$j++;
								endwhile;
							endif;
						endwhile; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>