<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#0072BC">
		<title><?php the_title();?></title>

		<!--Favicon-->
		<link rel="shortcut icon" href="<?php echo THEMEURL; ?>/assets/img/favicon.png" >
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-152x152.png">

		<?php include(TEMPLATEPATH . '/template-parts/loop-style.php'); ?>
		<?php wp_head();?>


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if IE ]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<link rel="stylesheet" type="text/css" href="ie.css" />
		<![endif]-->
	</head>
	<body <?php body_class();?>>
		<div class="menu-menor">
			<div class="logo-menor">
				<a href="<?php echo SITEURL; ?>">
					<img src="<?php echo THEMEURL.'/assets/img/daniel-annenberg.png' ?>" alt="Daniel Annenberg">
				</a>
			</div>
			<?php
		      	wp_nav_menu(
		      		array(
				        'menu'            => 'Header',
				 		'depth'           => 3,
				        'container_id'    => '',
				        'menu_class'      => 'cd-primary-nav is-fixed',
				        'menu_id'         => 'cd-primary-nav',
				        'echo'            => true,
				        'before'          => '',
				        'after'           => '',
				        'link_before'     => '',
				        'link_after'      => '',
				        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				        'walker'        => new themeslug_walker_nav_menu
		      		)
		      	);
	      	?>
			<div class="hamburger-menu">
				<span class="smally">Menu</span><div class="bar"></div>
			</div>
		</div>
		<div id="wrap" class="cd-main-content">
			<header class="header">
				<div class="box-menu">
					<div id="header-home">
						<div class="my-container">
							<div class="row">
								<div class="col-xs-12">
									<div class="desk-menu">
										<div class="hamburger-menu">
											<span class="smally">Menu</span><div class="bar"></div>
										</div>
										<div class="box-menu left">
											<div class="the-menu">
												<?php
											      	wp_nav_menu(
											      		array(
													        'menu'            => 'Header One',
													 		'depth'           => 3,
													        'container_id'    => '',
													        'menu_class'      => 'cd-primary-nav is-fixed',
													        'menu_id'         => '',
													        'echo'            => true,
													        'before'          => '',
													        'after'           => '',
													        'link_before'     => '',
													        'link_after'      => '',
													        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
													        'walker'        => new themeslug_walker_nav_menu
											      		)
											      	);
										      	?>
											</div>
										</div>
										<div class="logo">
											<?php
												if(is_front_page()){?>
													<h1 class="logo-adn">
														<a class="siteLogo" data-g-label="Daniel Annenberg Vereador" title="Daniel Annenberg Vereador" href="<?php echo SITEURL;?>">Daniel Annenberg Vereador</a>
													</h1>
												<?php }else{ ?>
													<span class="logo-adn">
														<a class="siteLogo" data-g-label="Daniel Annenberg Vereador" title="Daniel Annenberg Vereador" href="<?php echo SITEURL;?>">Daniel Annenberg Vereador</a>
													</span>
												<?php }
											?>
										</div>
										<div class="box-menu right">
											<div class="the-menu">
												<?php
											      	wp_nav_menu(
											      		array(
													        'menu'            => 'Header Two',
													 		'depth'           => 3,
													        'container_id'    => '',
													        'menu_class'      => 'cd-primary-nav is-fixed',
													        'menu_id'         => '',
													        'echo'            => true,
													        'before'          => '',
													        'after'           => '',
													        'link_before'     => '',
													        'link_after'      => '',
													        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
													        'walker'        => new themeslug_walker_nav_menu
											      		)
											      	);
										      	?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<a href="#">
							<div class="speak">
								<span>Fale com o </span>
								<span>Daniel</span>
							</div>
						</a>
					</div>
				</div>
			</header>
			<div class="conteudo">