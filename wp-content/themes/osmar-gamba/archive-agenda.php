<?php get_header() /* Template name: Agenda */;
include(TEMPLATEPATH.'/template-parts/titulo-comum.php'); 

setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');
$mesAtual = strftime('%B', strtotime('today'));
$mesAtual = ucfirst($mesAtual)
?>
<div class="menu-mes">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <?php
                    $agenda = array('post_type' => 'agenda',  'order' => 'ASC', 'posts_per_page' => -1 );
                    $mes = new WP_Query( $agenda );
                    while ( $mes->have_posts() ) : $mes->the_post();
                        $mesEvento = get_the_title(); ?>
                        <div class="mes">
                            <?php if (strcmp($mesAtual, $mesEvento) == 0):
                                echo '<span>'.criaResumo($mesEvento,3).'</span>';
                            else:
                                echo '<span><a href="'.get_the_permalink().'">'.criaResumo($mesEvento,3).'</a></span>';
                            endif; ?>
                        </div>
                    <?php endwhile;
                    wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="calendar">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10  col-lg-offset-2 col-lg-8">
                <?php
                $agenda = array('post_type' => 'agenda',  'order' => 'ASC', 'posts_per_page' => -1 );
                $mes = new WP_Query( $agenda );
                while ( $mes->have_posts() ) : $mes->the_post();
                    $mesEvento = get_the_title(); ?>
                    <?php if (strcmp($mesAtual, $mesEvento) == 0): ?>
                        <div class="the-calendar wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                            <?php
                            if( have_rows('sobre_evento') ):
                                $itera = 0; ?>
                                <div class="title">
                                    <h2><?php the_title(); ?></h2>
                                </div>
                                <?php while ( have_rows('sobre_evento') ) : the_row();
                                    if( get_row_layout() == 'evento' ): ?>
                                        <div class="event <?php if ($itera%2 == 0) echo 'gray'; $itera++;?> ">
                                            <div class="cover cover-<?php echo $itera; ?>"></div>
                                            <div class="thumb">
                                                <?php $thumbz = wp_get_attachment_image_src( get_post_thumbnail_id($case->ID), 'thumbnail' );
                                                $url_thumb = $thumbz['0']; ?>
                                                <?php if ($thumbz){ ?>
                                                    <img class="image" src="<?php echo $url_thumb; ?>" alt="<?php the_sub_field('nome_evento'); ?>">
                                                <?php }else{ ?>
                                                    <img class="image" src="<?php echo THEMEURL; ?>/assets/img/default.jpg" alt="<?php the_sub_field('nome_evento'); ?>">
                                                <?php } ?>
                                            </div>
                                            <div class="info">
                                                <div class="over"></div>
                                                <div class="date">
                                                    <?php 
                                                    $date = get_sub_field('data_evento', false, false);                                                
                                                    $date = new DateTime($date);
                                                    ?>
                                                    <span><?php echo $date->format('j'); ?></span>
                                                    <span><?php echo criaResumo($mesAtual,3); ?></span>
                                                </div>
                                                <div class="desc">
                                                    <div class="txt">
                                                        <h3><?php the_sub_field('nome_evento'); ?></h3>
                                                        <?php 
                                                        $dateFull = get_sub_field('data_evento', false, false);                                                
                                                        $dateFull = new DateTime($dateFull);
                                                        $hourFull = get_sub_field('horario_evento', false, false);                                                
                                                        $hourFull = new DateTime($hourFull);
                                                        ?>
                                                        <b><?php echo $dateFull->format('j/n/Y'); ?> - <?php the_sub_field('horario_evento'); if(get_sub_field('end_evento')): ?> <i class="daniel-dot"></i> <span><?php get_sub_field('end_evento')?></span><?php endif; ?></b>
                                                        <?php the_sub_field('desc_event'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php 
                                            $location = get_sub_field('place_event');
                                            if( !empty($location) ): ?>
                                                <div class="hidde-mapa">
                                                    <div class="mapa" id="mapa<?php echo $itera; ?>">
                                                        <div class="marker<?php echo $itera; ?>" id="marker<?php echo $itera; ?>" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif;
                                endwhile;
                                echo '<span class="hidden" id="total">'.$itera.'</span>';
                            else: ?>
                                <div class="nothing">
                                    <div class="title">
                                        <h2><?php the_title(); ?></h2>
                                        <h3>Não há eventos para esse mês</h3>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif ?>
                <?php endwhile;
                wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>