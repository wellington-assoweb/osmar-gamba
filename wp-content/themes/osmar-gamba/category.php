<?php get_header();
include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<div class="home-blog">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 side-desk">
                <?php include(TEMPLATEPATH . '/template-parts/search-side.php'); ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7"> 
                <?php $cont = 0;
                if(have_posts()) : while( have_posts() ) {
                    the_post();?>
                    <div class="col-xs-12 ultimas <?php if($cont %2 == 1) echo 'gray';?>" itemscope itemtype="http://schema.org/BlogPosting">
                        <div class="thumb">
                            <?php
                            $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); ?>
                            <a href="<?php echo the_permalink(); ?>">
                                <?php if ($img_post[0]){ ?>
                                    <img class="img-responsive" src="<?php echo $img_post[0] ?>" alt="<?php the_title(); ?>">
                                <?php }else{ ?>
                                    <img class="img-responsive" src="<?php echo THEMEURL.'/assets/img/default.jpg'; ?>" alt="<?php the_title(); ?>">
                                <?php } ?>
                            </a>
                        </div>
                        <div class="titulo-last">
                            <h3 itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <?php
                            $categoria = get_the_category();
                            foreach($categoria as $category) {
                                $output = '<a href="'.get_category_link( $category->term_id ).'" class="cat" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
                            }?>
                            <p class="data" ><?php the_date('d/m/Y'); ?> <i class="daniel-dot"></i> <?php echo $output; ?><span itemprop="author" class="hidden"><?php the_author_posts_link(); ?></span></p>
                        </div>
                        <div class="desc-last">
                            <p><a href="<?php echo the_permalink(); ?>"><?php the_content_limit(200); ?></a></p>
                            <div class="btn-daniel">
                                <a href="<?php the_permalink(); ?>" title="Continue lendo o artigo"><span>Continuar lendo</span></a>
                            </div>
                        </div>
                    </div>
                    <?php $cont++;
                } endif;?>
            </div>
            <div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-0 col-md-4 col-lg-3 side-mobile">
                <?php include(TEMPLATEPATH . '/template-parts/destaques.php'); ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>