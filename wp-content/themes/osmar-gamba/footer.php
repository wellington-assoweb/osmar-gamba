			</div>
			<?php echo do_shortcode('[contact-form-7 id="13" title="Formulário de contato" html_class="contact-form"]'); ?>
		</div>
		<footer class="footer" itemscope itemtype="http://schema.org/Organization">
			<div class="my-container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-2">
						<a href="<?php echo SITEURL; ?>"><img src="<?php echo THEMEURL.'/assets/img/daniel-annenberg-dark.png' ?>" class="image" alt="Daniel Annenberg Vereador"></a>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-7">
						<?php
							wp_nav_menu(
								array(
									'menu'           => 'Footer',
									'theme_location' => 'footer',
									'container'      => '',
									'menu_id'        => 'menu-footer',
									'menu_class'     => 'navbar-nav',
									'fallback_cb'    => 'wp_bootstrap_navwalker::fallback',
									'walker'            => new wp_bootstrap_navwalker()
								)
							);
						?>		
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3">
						<div class="social">
							<a href="https://www.facebook.com/danielannenberg/?fref=ts" target="_blank" title="Facebook - Daniel Annenberg"><i class="daniel-facebook"></i></a>
							<a href="https://twitter.com/danielannenberg" target="_blank" title="Twitter - Daniel Annenberg"><i class="daniel-twitter"></i></a>
							<a href="https://www.instagram.com/danielannenberg/" target="_blank" title="Instagram - Daniel Annenberg"><i class="daniel-instagram"></i></a>
							<a href="https://plus.google.com/u/0/+danielannenberg" target="_blank" title="Google Plus - Daniel Annenberg"><i class="daniel-gplus"></i></a>
							<a href="https://www.youtube.com/channel/UCbmfE2AUZ1yCJTqMTsElTmQ" target="_blank" title="Youtube - Daniel Annenberg"><i class="daniel-youtube-play"></i></a>
							<a href="https://www.linkedin.com/in/danielannenberg?authType=name&authToken=ts1U" target="_blank" title="LinkedIn - Daniel Annenberg"><i class="daniel-linkedin"></i></a>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="rights">
							<span>Daniel Annenberg <?php echo date('Y'); ?> &copy; Todos os direitos reservados <a href="http://www.assoweb.com.br" title="Agência Assoewb" target="blank"><i class="daniel-assoweb"></i></a></span>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<a href="#0" class="cd-top">Topo</a>
		<?php wp_footer();?>
	</body>
</html>