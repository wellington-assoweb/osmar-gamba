<?php get_header(); /* Template name: Biografia */ 
include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<section class="biografia">	
	<div class="about">
		<div class="over"></div>
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12">
					<div class="title">
						<h2>Um pouco sobre <span>Daniel Annenberg</span></h2>
					</div>
				</div>
				<div class="col-xs-12 col-sm-7 colmd-7 col-lg-8">
					<div class="contem">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="timeline">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="title">
					<h2><span>Linha do Tempo</span></h2>
				</div>
				<div id="cd-timeline" class="cd-container">
					<?php $cont = 1;
					if( have_rows('acontecimentos') ):
					    while ( have_rows('acontecimentos') ) : the_row();
					        if( get_row_layout() == 'acontecimento' ):
								if ($cont % 2 == 0): ?>
									<div class="cd-timeline-block right linhatempo">
										<div class="cd-timeline-img cd-picture is-hidden">
											<img src="<?php echo THEMEURL; ?>/assets/img/circle.png" alt="Círculo">
										</div>
										<div class="cd-timeline-content">
											<div class="titulo">
												<div class="box">
													<span><?php the_sub_field('ano'); ?></span>
												</div>
											</div>
											<div class="desc">
												<h3 class="titulo-acontece"><?php the_sub_field('titulo'); ?></h3>
												<?php the_sub_field('desc'); ?>
											</div>
										</div>
									</div>
								<?php else: ?>
									<div class="cd-timeline-block left linhatempo">
										<div class="cd-timeline-img cd-picture is-hidden">
											<img src="<?php echo THEMEURL; ?>/assets/img/circle.png" alt="Círculo">
										</div>
										<div class="cd-timeline-content">
											<div class="titulo">
												<div class="box some">
													<span><?php the_sub_field('ano'); ?></span>
												</div>
												<div class="box responsive">
													<span><?php the_sub_field('ano'); ?></span>
												</div>
											</div>
											<div class="desc">
												<h3 class="titulo-acontece"><?php the_sub_field('titulo'); ?></h3>
												<?php the_sub_field('desc'); ?>
											</div>
										</div>
									</div>
								<?php endif;
							$cont++; 
					        endif;
					    endwhile;
					endif;
					?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="publicacoes">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="title">
					<h2><span>Publicações</span></h2>
				</div>
				<div id="cd-timeline" class="cd-container">
					<?php $cont = 1;
					if( have_rows('todas_publicacoes') ):
					    while ( have_rows('todas_publicacoes') ) : the_row();
					        if( get_row_layout() == 'publicacao' ):
								if ($cont % 2 == 0): ?>
									<div class="cd-timeline-block right">
										<div class="cd-timeline-img cd-picture">
											<img src="<?php echo THEMEURL; ?>/assets/img/circle.png" alt="Círculo">
										</div>
										<div class="cd-timeline-content">
											<div class="titulo">
												<div class="box">
													<span><?php the_sub_field('ano'); ?></span>
												</div>
											</div>
											<div class="desc">
												<?php the_sub_field('desc'); ?>
											</div>
										</div>
									</div>
								<?php else: ?>
									<div class="cd-timeline-block left">
										<div class="cd-timeline-img cd-picture">
											<img src="<?php echo THEMEURL; ?>/assets/img/circle.png" alt="Círculo">
										</div>
										<div class="cd-timeline-content">
											<div class="titulo">
												<div class="box some">
													<span><?php the_sub_field('ano'); ?></span>
												</div>
												<div class="box responsive">
													<span><?php the_sub_field('ano'); ?></span>
												</div>
											</div>
											<div class="desc">
												<?php the_sub_field('desc'); ?>
											</div>
										</div>
									</div>
								<?php endif;
							$cont++; 
					        endif;
					    endwhile;
					endif;
					?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>