<?php get_header(); /* Template name: Propostas */  
include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<div class="menu-proposta">
	<div class="slide"><!-- ./owl-carousel-->
		<?php
		if( have_rows('propostas') ):
			$i = 1;
		    while ( have_rows('propostas') ) : the_row();
		        if( get_row_layout() == 'proposta' ): ?>
					<div class="item-proposta">
						<a href="#proposta<?php echo $i; ?>"><?php the_sub_field('nome_proposta'); ?></a>
		        	</div>
		        	<?php $i++;
		        endif;
		    endwhile;
		endif;
		?>
	</div>
</div>
<section class="todas-propostas">
	<?php
	if( have_rows('propostas') ):
		$j = 1;
		$time = .2;
	    while ( have_rows('propostas') ) : the_row();
	        if( get_row_layout() == 'proposta' ): ?>
				<div class="proposta <?php if($j % 2 == 1) echo'gray'; ?> wow fadeInUp" data-wow-duration="1s" data-wow-delay="<?php echo $time; ?>s" id="proposta<?php echo $j; ?>" >
					<div class="my-container">
						<div class="row">
							<div class="col-xs-12">
								<div class="title">
					        		<h2><?php the_sub_field('nome_proposta'); ?></h2>
								</div>
								<div class="nav-prop">
									<a class="prev-prop navega" href="#proposta<?php echo $j-1; ?>"><i class="daniel-left"></i> Anterior</a>
									<a class="next-prop navega" href="#proposta<?php echo $j+1; ?>">Próxima <i class="daniel-right"></i></a>
								</div>
								<div class="desc">
					        		<?php the_sub_field('desrc_proposta'); ?>
								</div>
								<?php if (get_sub_field('video_proposta')): ?>
									<div class="video <?php if (!get_sub_field('infografico')) echo 'center'; ?>">
										<iframe width="420" height="315" src="https://www.youtube.com/embed/<?php the_sub_field('video_proposta'); ?>"></iframe>
									</div>
								<?php endif ?>
								<?php if (get_sub_field('infografico')): ?>
									<div class="infografico <?php if (!get_sub_field('video_proposta')) echo 'center'; ?>">
						        		<?php $image = get_sub_field('infografico');
										if( !empty($image) ): ?>
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										<?php endif; ?>
									</div>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
	        <?php endif;
			$j++;
			$time += .2;
	    endwhile;
	endif;
	?>
</section>
<?php get_footer(); ?>