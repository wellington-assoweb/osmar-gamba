<?php get_header(); /* Template name: Fale com o Daniel */ 
include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<section class="fale">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-sm-offset-3 col-sm-6">
				<div class="o-form">
					<div class="contem">
						<?php the_content(); ?>
					</div>
					<?php echo do_shortcode('[contact-form-7 id="13" title="Formulário de contato" html_class="fale-daniel"]'); ?>
				</div>
			</div>
		</div>
	</div>			
</section>
<?php get_footer(); ?>