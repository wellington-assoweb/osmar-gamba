<?php get_header();
include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>
<div class="search-blog">
    <div class="container">
    	<div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-1 col-lg-10 ">
	        	<?php if ( have_posts() ) : 
	        		$conta = 1?>
	        		<div class="title">
						<h2>Resultados para: <span><?php echo $_GET["s"] ?></span></h2>
	        		</div>
					<form id="formheader" method="get" action="<?php echo SITEURL; ?>/">
						<input class="sb-search-input" placeholder="Pesquise aqui" type="text" value="" name="s" id="s">
						<input class="sb-search-submit" type="submit" value="Buscar">
						<span class="sb-icon-search"></span>
					</form>
	        		<?php while ( have_posts() ) : the_post(); ?>
	        			<div class="resultados-busca <?php if($conta %2 === 0) echo 'gray';?>">	        				
		        			<div class="thumb">
		                        <?php
		                        $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); ?>
		                        <a href="<?php echo the_permalink(); ?>">
		                            <?php if ($img_post[0]){ ?>
		                                <img class="img-responsive" src="<?php echo $img_post[0] ?>" alt="<?php the_title(); ?>">
		                            <?php }else{ ?>
		                                <img class="img-responsive" src="<?php echo THEMEURL.'/assets/img/default.jpg'; ?>" alt="<?php the_title(); ?>">
		                            <?php } ?>
		                        </a>
		                    </div>
		                    <div class="titulo-last">
		                        <h3 itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		                        <?php
		                        $categoria = get_the_category();
		                        foreach($categoria as $category) {
		                            $output = '<a href="'.get_category_link( $category->term_id ).'" class="cat" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
		                        }?>
		                        <p class="data" ><?php the_date('d/m/Y'); ?> <i class="daniel-dot"></i> <?php echo $output; ?><span itemprop="author" class="hidden"><?php the_author_posts_link(); ?></span></p>
		                    </div>
		                    <div class="desc-last">
		                        <a href="<?php echo the_permalink(); ?>"><?php the_content_limit(200); ?></a>
		                        <div class="btn-daniel">
		                            <a href="<?php the_permalink(); ?>" title="Continue lendo o artigo"><span>Continuar lendo</span></a>
		                        </div>
		                    </div>
	        			</div>
						<?php $conta++;
					endwhile; ?>
				<?php else: ?>
					<div class="erro">
	        			<div class="title">
							<h2>Busca por <span><?php echo get_search_query();?></span> não encontrada</h2>
						</div>
						<p>Tente novamente</p>
						<form id="formheader" method="get" action="<?php echo SITEURL; ?>/">
							<input class="sb-search-input" placeholder="Pesquise aqui" type="text" value="" name="s" id="s">
							<input class="sb-search-submit" type="submit" value="Buscar">
							<span class="sb-icon-search"></span>
						</form>
					</div>
				<?php endif;
				wp_reset_postdata(); ?>
	        </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>