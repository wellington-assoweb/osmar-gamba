<?php

  /* Insert ACF Google maps API*/
  add_filter('acf/settings/google_api_key', function () {
      return 'AIzaSyDtFuqftFj3DT2MmnEn8YupC0DMQEy0VuM';
  });
/*=========================================================================================
LOAD SCRIPTS AND STYLES
=========================================================================================*/
add_action('wp_enqueue_scripts', 'sample_scripts');
function sample_scripts() {
  //PEGA O DIRETÓRIO DEFAULT DOS ARQUIVOS PARA CONCATENAR
  define(CSS_PATH, THEMEURL.'/assets/css/');
  define(JS_PATH, THEMEURL.'/assets/js/');

  wp_enqueue_script('jquery');

  wp_register_style('bootstrap', CSS_PATH.'bootstrap.min.css', null, null, 'all' );
  wp_register_style('style', CSS_PATH.'style.css', null, null, 'all' );
  wp_register_style('osmar-font', CSS_PATH.'osmar.css', null, null, 'all' );
  wp_register_style('animate-css', CSS_PATH.'animate.css', null, null, 'all' );

  wp_enqueue_style('bootstrap');
  wp_enqueue_style('osmar-font');


  //Declaração de scripts
  wp_register_script('bootstrap_js', JS_PATH.'bootstrap.min.js', null, null, true );
  //wp_register_script('mailchimp','//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js', null, null, true );
  wp_register_script('smooth-scroll', JS_PATH.'smooth-scroll.min.js', null, null, true );
  wp_register_script('scripts', JS_PATH.'scripts.min.js', null, null, true );
  wp_register_script('formulario-js', JS_PATH.'formulario.min.js', null, null, true );

  wp_enqueue_script('bootstrap_js');
  //wp_enqueue_script('mailchimp');
  wp_enqueue_script('smooth-scroll');
  wp_enqueue_style('animate-css');
  wp_enqueue_style('style');


  /*FIM MENU*/
  if(is_front_page()){
    wp_register_style('owl-carousel', CSS_PATH.'owl.carousel.css', null, null, 'all' );
    wp_enqueue_style('owl-carousel');

    wp_register_style('front-page-css', CSS_PATH.'front-page.css', null, null, 'all' );
    wp_enqueue_style('front-page-css');

    wp_register_style('animate', CSS_PATH.'animate.css', null, null, 'all' );
    wp_enqueue_style('animate');

    wp_register_script('wow-js', JS_PATH.'wow.min.js', null, null, true );
    wp_enqueue_script('wow-js');

    wp_register_script('carousel-js', JS_PATH.'owl.carousel.min.js', null, null, true );
    wp_enqueue_script('carousel-js');

    wp_register_script('home-js', JS_PATH.'home.js', null, null, true );
    wp_enqueue_script('home-js');

  }elseif(is_page()){

    if(is_page(98)){ // Biografia
      wp_register_style('biografia-css', CSS_PATH.'biografia.css', null, null, 'all' );
      wp_enqueue_style('biografia-css');

      wp_register_script('biografia-js', JS_PATH.'biografia.min.js', null, null, true );
      wp_enqueue_script('biografia-js');

    }elseif(is_page(498)){ // Propostas
      wp_register_style('propostas-css', CSS_PATH.'propostas.css', null, null, 'all' );
      wp_enqueue_style('propostas-css');

      wp_register_style('owl-carousel', CSS_PATH.'owl.carousel.css', null, null, 'all' );
      wp_enqueue_style('owl-carousel');

      wp_register_script('carousel-default-js', JS_PATH.'owl.theme.default.css', null, null, true );
      wp_enqueue_script('carousel-default-js');

      wp_register_style('animate', CSS_PATH.'animate.css', null, null, 'all' );
      wp_enqueue_style('animate');

      wp_register_script('wow-js', JS_PATH.'wow.min.js', null, null, true );
      wp_enqueue_script('wow-js');

      wp_register_script('carousel-js', JS_PATH.'owl.carousel.min.js', null, null, true );
      wp_enqueue_script('carousel-js');

      wp_register_script('propostas-js', JS_PATH.'propostas.min.js', null, null, true );
      wp_enqueue_script('propostas-js');

    }elseif(is_page(560)){ // Fale com o Daniel
      wp_register_style('contato-css', CSS_PATH.'contato.css', null, null, 'all' );
      wp_enqueue_style('contato-css');

    }

  }elseif(is_post_type_archive('agenda') || is_singular('agenda') || is_page(517)) {
      wp_register_style('agenda-css', CSS_PATH.'agenda.css', null, null, 'all' );
      wp_enqueue_style('agenda-css');

      wp_register_style('animate', CSS_PATH.'animate.css', null, null, 'all' );
      wp_enqueue_style('animate');

      wp_register_script('wow-js', JS_PATH.'wow.min.js', null, null, true );
      wp_enqueue_script('wow-js');

      wp_register_script( 'googlemaps-api', '//maps.googleapis.com/maps/api/js?v=3&key=AIzaSyDtFuqftFj3DT2MmnEn8YupC0DMQEy0VuM&signed_in=true', null, null, false);
      wp_enqueue_script( 'googlemaps-api' );

      wp_register_script('my-query-js', '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', null, null, true );
      wp_enqueue_script('my-query-js');

      //wp_enqueue_script( 'google-map-init', get_template_directory_uri() . '/library/js/google-maps.js', array('google-map', 'jquery'), '0.1', true );

      wp_register_script('agenda-js', JS_PATH.'agenda.min.js', null, null, true );
      wp_enqueue_script('agenda-js');

  }elseif (is_post_type_archive('midia') || is_singular('midia')) {
      wp_register_style('midia-single-css', CSS_PATH.'midia-single.css', null, null, 'all' );
      wp_enqueue_style('midia-single-css');

      if (is_singular('midia')) {
        wp_register_script('jquery-fancybox-pack-js', JS_PATH.'jquery-fancybox-pack.min.js', null, null, true );
        wp_enqueue_script('jquery-fancybox-pack-js');

        wp_register_script('jquery-fancybox-media-js', JS_PATH.'jquery-fancybox-media.min.js', null, null, true );
        wp_enqueue_script('jquery-fancybox-media-js');

        wp_register_script('midia-js', JS_PATH.'midia.min.js', null, null, true );
        wp_enqueue_script('midia-js');

      }elseif(is_post_type_archive('midia')){
        wp_register_style('midia-css', CSS_PATH.'midia.css', null, null, 'all' );
        wp_enqueue_style('midia-css');

        wp_register_script('select', JS_PATH.'select-fotografia.js', null, null, true );
        wp_enqueue_script('select');
        wp_register_script('mix', JS_PATH.'mixitup.min.js', null, null, true );
        wp_enqueue_script('mix');

        wp_register_script('video-midia-js', JS_PATH.'video-midia.min.js', null, null, true );
        wp_enqueue_script('video-midia-js');

      }

  }elseif(is_home() || is_category() || is_single() || is_author() || is_search()){
    wp_register_style('blog-css', CSS_PATH.'blog.css', null, null, 'all' );
    wp_enqueue_style('blog-css');

    if(is_search()){
      wp_register_style('search-css', CSS_PATH.'search.css', null, null, 'all' );
      wp_enqueue_style('search-css');

    }elseif(is_single()){

      // DISQUS
      function disqus_embed($disqus_shortname) {
        global $post;
        wp_enqueue_script('disqus_embed','http://'.$disqus_shortname.'.disqus.com/embed.js');
        echo '<div id="disqus_thread"></div>
        <script type="text/javascript">
          var disqus_shortname = "'.$disqus_shortname.'";
          var disqus_title = "'.$post->post_title.'";
          var disqus_url = "'.get_permalink($post->ID).'";
          var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
        </script>';
      }
    }

    wp_register_script('blog-js', JS_PATH.'blog.min.js', null, null, true );
    wp_enqueue_script('blog-js');

  }elseif(is_404()){
    wp_register_style('404-css', CSS_PATH.'404.min.css', null, null, 'all' );
    wp_enqueue_style('404-css');

  }

  //Chamada de scripts
  wp_enqueue_script('scripts');
  wp_enqueue_script('formulario-js');

  wp_register_script('scripts-js', JS_PATH.'scripts.js', null, null, true );
  wp_enqueue_script('scripts-js');

}




/*Deregister styles and scripts
================================================================================================================*/
function deregister_styles() {

    if (!is_home() && !is_single() && !is_category() && !is_tag() && !is_author()) {
        wp_deregister_style('jetpack-whatsapp');
        wp_deregister_style( 'wp-pagenavi' );
    }
    // if (!is_page('contato')) {
    //     wp_deregister_style( 'contact-form-7' );
    // }
}
add_filter( 'wp_print_styles', 'deregister_styles' );

//Deregister styles and scripts
//================================================================================================================
function deregister_scripts() {
    // if (!is_page('contato')) {
    //     wp_deregister_script( 'contact-form-7' );
    // }
    if (!is_home() && !is_single() && !is_category() && !is_tag() && !is_author()) {
        wp_deregister_script('jetpack-whatsapp');
    }
}
add_filter( 'wp_print_scripts', 'deregister_scripts' );

/*Deregister Contact Form 7 styles
================================================================================================================*/

// Limit search for only blog posts
function SearchFilter($query) {
  if ($query->is_search && !is_admin()) {
      if (isset($query->query["post_type"])) {
          $query->set('post_type', $query->query["post_type"]);
      } else {
          $query->set('post_type', 'post');
      }
  }
  return $query;
}
add_filter('pre_get_posts','SearchFilter');


// Thumbnails custom sizes
function wpdocs_theme_setup() {
    add_image_size( 'large-desktop', 1200 );
    add_image_size( 'tv-resolution', 1600 );
}
add_action( 'after_setup_theme', 'wpdocs_theme_setup' );

//FUNÇÃO PARA SUPORTE A UPLOAD DE IMAGENS SVG
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

include(TEMPLATEPATH . '/template-parts/walker.php');

// // Aumenta limite de upload
// @ini_set( 'upload_max_size' , '64M' );
// @ini_set( 'post_max_size', '64M');
// @ini_set( 'max_execution_time', '300' );


//add_filter( 'jetpack_development_mode', '__return_true' );

//ADICIONA SUPORTE A MENUS NO PAINEL ADMIN
add_theme_support('menus');
//ADICIONA SUPORTE POST FORMATS
add_theme_support( 'post-formats', array( 'video' ) );
//REMOVE BARRA DE ADMIN
add_filter('show_admin_bar', '__return_false');
//ENABLE THUMBS
add_theme_support('post-thumbnails');
//UNABLE LOGIN SHOW ERRORS
add_filter('login_errors',create_function('$a', "return null;"));
//REMOVE HEAD VERSION
remove_action('wp_head', 'wp_generator');
//ENABLE THUMBS
add_theme_support( 'post-thumbnails' );


// function tweakjp_rm_comments_att( $open, $post_id ) {
//   $post = get_post( $post_id );
//   if( $post->post_type == 'attachment' ) {
//       return false;
//   }
//   return $open;
// }
// add_filter( 'comments_open', 'tweakjp_rm_comments_att', 10 , 2 );

require_once('wp_bootstrap_navwalker.php');
//require_once('wlaker_dropdown.php');
register_nav_menus(
  array(
    'header'        => __('Header'),
    'footer'        => __('Footer')
  )
);


/*=======================================================================================
CUSTOM FUNCTION: LIMIT TEXT
=======================================================================================*/
function the_content_limit($max_char, $conteudo = 'the_content', $more_link_text = '', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters($conteudo, $content);
    $content = str_replace(']]>', ']]>', $content);

   if (strlen($_GET['']) > 0) {
      echo $content;
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
      $content = substr($content, 0, $espacio);
      echo $content.'<a href="'.get_permalink().'"> Leia mais...</a>';
   }
   else {
      echo $content;
   }
}


/*=======================================================================================
CUSTOM FUNCTION: RESUME WORDS
=======================================================================================*/
function criaResumo($string,$caracteres) {
    $string = strip_tags($string);
    $cont = 1;
    if (strlen($string) > $caracteres) {
        while (substr($string,$caracteres,1) <> ' ' && ($cont < strlen($string))){
            $cont++;
        };
    };
    return substr($string,0,$caracteres);
}


//CUSTOM POST TYPE MÍDIA
add_action('init', 'type_post_midia');
function type_post_midia() {
  $labels = array(
    'name'                => _x( 'Mídia Osmar', 'Post Type General Name', 'osmar-gamba' ),
    'singular_name'       => _x( 'Mídia Osmar', 'Post Type Singular Name', 'osmar-gamba' ),
    'menu_name'           => __( 'Mídia Osmar', 'osmar-gamba' ),
    'parent_item_colon'   => __( 'Parent Item:', 'osmar-gamba' ),
    'all_items'           => __( 'Todos as Mídias', 'osmar-gamba' ),
    'view_item'           => __( 'Ver Mídia Osmar', 'osmar-gamba' ),
    'add_new_item'        => __( 'Add nova Mídia Osmar', 'osmar-gamba' ),
    'add_new'             => __( 'Add nova', 'osmar-gamba' ),
    'edit_item'           => __( 'Editar Mídia Osmar', 'osmar-gamba' ),
    'update_item'         => __( 'Atualizar Mídia Osmar', 'osmar-gamba' ),
    'search_items'        => __( 'Pesquisar Mídia Osmar', 'osmar-gamba' ),
    'not_found'           => __( 'Nada encontrado', 'osmar-gamba' ),
    'not_found_in_trash'  => __( 'Nada encontrado na lixeira', 'osmar-gamba' ),
    );
  $rewrite = array(
    'slug'                => 'midia'
    );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'public_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => $rewrite,
    'capability_type' => 'post',
    'has_archive' => true,
    'menu_icon' => 'dashicons-money',
    'hierarchical' => false,
    'menu_position' => null,

    'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions')
    );

    register_post_type( 'midia' , $args );
    flush_rewrite_rules();
}

// TAXONOMY PARA MÍDIA
function midia_taxonomy() {
  register_taxonomy( 'categorias-midia', 'midia',
    array(
      'labels' => array(
          'name'              => 'Categorias de mídia',
          'singular_name'     => 'Categoria de mídia',
          'search_items'      => 'Pesquisar Categorias de mídia',
          'all_items'         => 'Todas as Categorias',
          'edit_item'         => 'Editar Categorias',
          'update_item'       => 'Atualizar Categorias',
          'add_new_item'      => 'Adicionar nova Categoria',
          'new_item_name'     => 'Nome da Categoria',
          'menu_name'         => 'Categorias',
      ),
      'hierarchical' => true,
      'supports'            => array( 'thumbnail' ),
      'show_admin_column' => true
    )
  );
}
add_action( 'init', 'midia_taxonomy', 0);

//CUSTOM POST TYPE AGENDA
add_action('init', 'type_post_agenda');
function type_post_agenda() {
  $labels = array(
    'name'                => _x( 'Agenda', 'Post Type General Name', 'osmar-gamba' ),
    'singular_name'       => _x( 'Agenda', 'Post Type Singular Name', 'osmar-gamba' ),
    'menu_name'           => __( 'Agenda', 'osmar-gamba' ),
    'parent_item_colon'   => __( 'Parent Item:', 'osmar-gamba' ),
    'all_items'           => __( 'Todas as agendas', 'osmar-gamba' ),
    'view_item'           => __( 'Ver Agenda', 'osmar-gamba' ),
    'add_new_item'        => __( 'Add nova Agenda', 'osmar-gamba' ),
    'add_new'             => __( 'Add nova', 'osmar-gamba' ),
    'edit_item'           => __( 'Editar Agenda', 'osmar-gamba' ),
    'update_item'         => __( 'Atualizar Agenda', 'osmar-gamba' ),
    'search_items'        => __( 'Pesquisar Agenda', 'osmar-gamba' ),
    'not_found'           => __( 'Nada encontrado', 'osmar-gamba' ),
    'not_found_in_trash'  => __( 'Nada encontrado na lixeira', 'osmar-gamba' ),
    );
  $rewrite = array(
    'slug'                => 'agenda'
    );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'public_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => $rewrite,
    'capability_type' => 'post',
    'has_archive' => true,
    'menu_icon' => 'dashicons-calendar-alt',
    'hierarchical' => false,
    'menu_position' => null,

    'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'trackbacks')
    );

    register_post_type( 'agenda' , $args );
    flush_rewrite_rules();
}

// TAXONOMY PARA AGENDA
function agenda_taxonomy() {
  register_taxonomy( 'mes', 'agenda',
    array(
      'labels' => array(
          'name'              => 'Categorias de mídia',
          'singular_name'     => 'Categoria de mídia',
          'search_items'      => 'Pesquisar Categorias de mídia',
          'all_items'         => 'Todas as Categorias',
          'edit_item'         => 'Editar Categorias',
          'update_item'       => 'Atualizar Categorias',
          'add_new_item'      => 'Adicionar nova Categoria',
          'new_item_name'     => 'Nome da Categoria',
          'menu_name'         => 'Categorias',
      ),
      'hierarchical' => true,
      'supports'            => array( 'thumbnail' ),
      'show_admin_column' => true
    )
  );
}
add_action( 'init', 'agenda_taxonomy', 0);



/*ADICIONA FAVICON À TELA DE LOGIN E AO PAINEL DO SITE*/
add_action( 'login_head', 'favicon_admin' );
add_action( 'admin_head', 'favicon_admin' );
add_filter( 'wpcf7_support_html5', '__return_false' );
function favicon_admin() {
    $favicon_url = THEMEURL . '/assets/img/favicon';
    $favicon  = '<!-- Favicon IE 9 -->';
    $favicon .= '<!--[if lte IE 9]><link rel="icon" type="image/x-icon" href="' . $favicon_url . '.ico" /> <![endif]-->';
    $favicon .= '<!-- Favicon Outros Navegadores -->';
    $favicon .= '<link rel="shortcut icon" type="image/png" href="' . $favicon_url . '.png" />';
    $favicon .='<!-- Favicon iPhone -->';
    $favicon .='<link rel="apple-touch-icon" href="' . $favicon_url . '.png" />';
    echo $favicon;
}

/*=======================================================================================
STYLE FOR LOGIN PAGE
=======================================================================================*/
//Link na tela de login para a página inicial
// Custom WordPress Login Logo
function my_login_logo() {
  echo '
    <style type="text/css">
      html{
        background-color: #000;
      }
      .login #login_error{
        display: none;
      }
      body.login div#login h1 a {
        pointer-events: none;
        background-image: url('.THEMEURL.'/assets/img/daniel-annenberg-white.png);
        padding-bottom: 0px;
        background-size: contain;
        width: 270px;
      }
      body.login {
        background-image: url('.THEMEURL.'/assets/img/daniel-annenberg-login.jpg);
        background-size: cover;
        background-repeat: no-repeat;
        background-position: 50%;
      }
      .login form {
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
        background:#FFF;
        opacity:0.90;
        border-radius:5px;
      }
      .login #backtoblog a, .login #nav a {
        text-decoration: none;
        color: #FFF !important;
        font-weight:bold;
      }
      .login label {
        color: #0072bc !important;
        text-transform: uppercase;
        font-size: 14px;
      }
      .login input{
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -ms-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
      }
      .wp-core-ui .button.button-large {
        height: 51px !important;
        line-height: 28px;
        margin-top:15px;
        color: #FFF;
        border-color: transparent !important;
        padding: 0px 12px 2px;
        width: 100%;
        background: #0072bc none repeat scroll 0% 0% !important;
        border-radius: 0px;
        text-transform: uppercase;
      }
      .wp-core-ui .button.button-large:hover{
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
      }
      .login input[type=text]:focus,
      .login input[type=password]:focus{
        -webkit-box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
        -moz-box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
        box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
      }
    </style>
  ';
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );



/*MAIS LIDAS
======================*/
// Verifica se não existe nenhuma função com o nome post_count_session_start
if ( ! function_exists( 'post_count_session_start' ) ) {
    // Cria a função
    function post_count_session_start() {
        if ( ! session_id() ) session_start(); // Inicia uma sessão PHP
    }
    add_action( 'init', 'post_count_session_start' ); // Executa a ação
}

// Verifica se não existe nenhuma função com o nome tp_count_post_views
if ( ! function_exists( 'tp_count_post_views' ) ) {
    // Conta os views do post
    function tp_count_post_views () {
        // Garante que vamos tratar apenas de posts
        if ( is_single() ) {
            global $post; // Precisamos da variável $post global para obter o ID do post

            // Se a sessão daquele posts não estiver vazia
            if ( empty( $_SESSION[ 'tp_post_counter_' . $post->ID ] ) ) {


                $_SESSION[ 'tp_post_counter_' . $post->ID ] = true;  // Cria a sessão do posts

                // Cria ou obtém o valor da chave para contarmos
                $key = 'tp_post_counter';
                $key_value = get_post_meta( $post->ID, $key, true );

                // Se a chave estiver vazia, valor será 1
                if ( empty( $key_value ) ) { // Verifica o valor
                    $key_value = 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } else {
                    $key_value += 1; // Caso contrário, o valor atual + 1
                    update_post_meta( $post->ID, $key, $key_value );
                } // Verifica o valor

            } // Checa a sessão

        } // is_single
        return;
      }
    add_action( 'get_header', 'tp_count_post_views' );
}

function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      //echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }

}


