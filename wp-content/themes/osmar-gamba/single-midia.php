<?php get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>

<section class="midia single">
	<div class="my-container">
		<div class="row">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
				<div class="col-xs-12">
					<div class="title">
						<h1><?php the_title(); ?></h1>
					</div>
					<div class="gallery">
						<?php 
                       	$contem = get_the_content();
						$categorias = get_the_terms($post->ID, 'categorias-midia');
                        foreach ($categorias as $cat) {
                            $cats.= $cat->slug.' ';
                        };
                        if(strpos($cat->slug,'imagens') !== false): 
                       		if ($contem): ?>
								<div class="description">
									<?php the_content(); ?>
								</div>
                       		<?php endif; ?>
				            <ul>
				            	<?php 
								$images = get_field('galeria_imagem');
								if( $images ): 
									foreach( $images as $image ): ?>
							            <li>
						                    <a class="fancybox" rel="gallery1" href="<?php echo $image['sizes']['large']; ?>"
						                       title="<?php echo $image['caption']; ?>">
						                       	<div class="box">
						                        	<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['caption']; ?>" />
						                       	</div>
						                        <p><?php echo $image['description']; ?></p>
						                    </a>
							            </li>
							        <?php endforeach;
							    endif; ?>
				            </ul>
                       	<?php else:	
                       		if ($contem): ?>
								<div class="description">
									<?php the_content(); ?>
								</div>
                       		<?php endif; ?>
                       		<div class="video-frame">
                       			<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php the_field('id_video'); ?>"></iframe>
                       		</div>
                        <?php endif; ?>
					</div>
				</div>
			<?php endwhile; endif;?>
		</div>
	</div>
</section>
<?php get_footer(); ?>